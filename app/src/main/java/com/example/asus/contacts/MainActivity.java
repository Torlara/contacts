package com.example.asus.contacts;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static final String CONTACTS = "contacts";
    static final String SEARCH_OPENED = "searchOpened";
    static final String SEARCH_QUERY = "searchQuery";
    static final String INDEX_START = "indexStart";
    static final String COUNT = "count";

    private ArrayList<String> mContacts = new ArrayList<String>();
    private boolean mSearchOpened;
    private String mSearchQuery;
    private int mIndexStart,  mIndexCount;

    private ListView mContactsLv;
    private Drawable mIconOpenSearch;
    private Drawable mIconCloseSearch;
    private EditText mSearchEt;
    private MenuItem mSearchAction;

    private ContactsListAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            try {

                //getting contacts to fill the list
                InputStream inputreader = getAssets().open("contacts.txt");
                BufferedReader buffreader = new BufferedReader(new InputStreamReader(inputreader));

                while (true) {
                    String line = buffreader.readLine();
                    if (line == null)
                        break;
                    mContacts.add(line);
                }

            } catch (java.io.IOException e) {
                e.printStackTrace();
            }

            mSearchOpened = false;
            mSearchQuery = "";
            mIndexStart = -1;
            mIndexCount = mContacts.size()+1;

        } else {
            mContacts = savedInstanceState.getStringArrayList(CONTACTS);
            mSearchOpened = savedInstanceState.getBoolean(SEARCH_OPENED);
            mSearchQuery = savedInstanceState.getString(SEARCH_QUERY);
            mIndexStart = savedInstanceState.getInt(INDEX_START);
            mIndexCount = savedInstanceState.getInt(COUNT);
        }

        //getting the icons
        mIconOpenSearch = getResources().getDrawable(R.drawable.ic_search);
        mIconCloseSearch = getResources().getDrawable(R.drawable.ic_clear);

        //initializing listview
        mContactsLv = (ListView) findViewById(R.id.lvContacts);

        //setting the listed adapter (list within range from mIndexStart to mIndexCount)
        mAdapter = new ContactsListAdapter(mContacts, this, mIndexStart, mIndexCount);
        mContactsLv.setAdapter(mAdapter);
        mContactsLv.setEmptyView(findViewById(R.id.emptyTxt));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
     public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);

        //if the search bar was opened previously, open it on recreate
        if (mSearchOpened) {
            openSearchBar(mSearchQuery);
        }

        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            if (mSearchOpened) {
                closeSearchBar();
            } else {
                openSearchBar(mSearchQuery);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openSearchBar(String queryText) {
        //set custom view on action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.search_bar);
        actionBar.setDisplayShowTitleEnabled(false);

        //add editText to customView
        mSearchEt = (EditText) actionBar.getCustomView().findViewById(R.id.etSearch);
        mSearchEt.addTextChangedListener(new SearchWatcher());
        mSearchEt.setText(queryText);
        mSearchEt.requestFocus();

        //change search icon if menu is opened
        mSearchAction.setIcon(mIconCloseSearch);
        mSearchOpened = true;

        //show keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mSearchEt, InputMethodManager.SHOW_IMPLICIT);

    }

    private void closeSearchBar() {
        //delete custom view
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);

        mSearchEt.setText("");
        //change search icon if menu is closed
        mSearchAction.setIcon(mIconOpenSearch);
        mSearchOpened = false;

        //hide keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchEt.getWindowToken(), 0);

    }

    //handles changes in search edit text
    private class SearchWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence c, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence c, int i, int i2, int i3) {
            mSearchQuery = mSearchEt.getText().toString();
            mAdapter.search(mSearchQuery);
            mIndexCount = mAdapter.mIndexCount;
            mIndexStart = mAdapter.mIndexStart;
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putStringArrayList(CONTACTS, mContacts);
        savedInstanceState.putBoolean(SEARCH_OPENED, mSearchOpened);
        savedInstanceState.putString(SEARCH_QUERY, mSearchQuery);
        savedInstanceState.putInt(INDEX_START, mIndexStart);
        savedInstanceState.putInt(COUNT, mIndexCount);

    }

}

