package com.example.asus.contacts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by asus on 2015-10-14.
 */
public class ContactsListAdapter extends BaseAdapter {
    private ArrayList<String> mStringList;
    private LayoutInflater mInflater;
    public int mIndexStart, mIndexStop, mIndexCount;

    public ContactsListAdapter(ArrayList<String> stringList, Context context) {
        mStringList = stringList;
        mInflater = LayoutInflater.from(context);

        mIndexStart = -1;
        mIndexStop = mStringList.size();
        mIndexCount = mStringList.size()+1;

        //Log.w("start: "+mIndexStart, "stop: "+mIndexStop);

    }
    public ContactsListAdapter(ArrayList<String> stringList, Context context, int indexStart, int count) {
        mStringList = stringList;
        mInflater = LayoutInflater.from(context);

        mIndexStart = indexStart;
        mIndexStop = mStringList.size();
        mIndexCount = count;

        //Log.w("start: "+mIndexStart, "stop: "+mIndexStop);

    }

    //overriding getCount, getItem, getItem Id and getView to show range of filtered contacts
    @Override
    public int getCount() {
        return mIndexCount - 1;
    }

    @Override
    public Object getItem(int position) {
        return mStringList.get(mIndexStart + 1 + position);
    }

    @Override
    public long getItemId(int position) {
        return mIndexStart + 1 + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder viewHolder;
        if(convertView == null) {
            viewHolder = new Holder();
            convertView = mInflater.inflate(R.layout.list_item,null);
            viewHolder.nameTv = (TextView)convertView.findViewById(R.id.txtListItem);
            viewHolder.avatar = (ImageView)convertView.findViewById(R.id.avatar);
            viewHolder.avatar.getDrawable().setColorFilter(Color.parseColor("#3F51B5"), PorterDuff.Mode.MULTIPLY);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (Holder)convertView.getTag();
        }
            //we have to add 1, because from search method we get range between which we have our filtered list
            viewHolder.nameTv.setText(mStringList.get(mIndexStart + 1 + position));


        return convertView;
    }

    //searching method
    public void search (String query){
        //search using binary search
        final int index = Collections.binarySearch(mStringList, query, comparator);

        //if elemenent with wanted prefix is in the list
        if(index > -1) {
            mIndexStart = index;
            mIndexStop = index;

            //iterate to left and right to found its range (binarySearch gives random element, we have to found its start and end)
            while (mIndexStart > -1 && mStringList.get(mIndexStart).toLowerCase().startsWith(query.toLowerCase()))
                mIndexStart--;

            while (mIndexStop < mStringList.size() && mStringList.get(mIndexStop).toLowerCase().startsWith(query.toLowerCase()))
                mIndexStop++;

            mIndexCount = mIndexStop - mIndexStart;
            notifyDataSetChanged();
        } else {
            mIndexCount = 1;
            notifyDataSetChanged();
        }

        //Log.w("start: "+mIndexStart, "stop: "+mIndexStop);
    }
    private class Holder {
        TextView nameTv;
        ImageView avatar;
    }

    //comparator for binarySearch to search only by given prefix
    Comparator<String> comparator = new Comparator<String>() {
        public int compare(String currentItem, String key) {
            if(currentItem.toLowerCase().startsWith(key.toLowerCase())) {
                return 0;
            }
            return currentItem.toLowerCase().compareTo(key.toLowerCase());
        }
    };


}